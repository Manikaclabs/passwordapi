var express = require('express');
var router = express.Router();
var md5 = require('MD5');
var async = require('async');
var func = require('./commonFunction');
var sendResponse = require('./sendResponse');
var passwordGenerate = require('password-generator');
/*
 * --------------------------------------------------------------------------
 * sign_up
 * INPUT : firstName, lastName, email, password
 * OUTPUT : error, userResult
 * ---------------------------------------------------------------------------
 */
router.post('/sign_up', function (req, res, next) {

    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var email = req.body.email;
    var password = req.body.password;
    var manValues = [firstName, lastName, email, password];
    async.waterfall([
        function (callback) {
            func.checkBlank(res, manValues, callback);
        },
        function (callback) {
            func.checkEmailAvailability(res, email, callback);
        }],
            function (resultCallback) {
                var loginTime = new Date();
                var accessToken = func.encrypt(email + loginTime);
                var encryptPassword = md5(password);
                var sql = "INSERT into users(email,password,access_token,last_login,first_name,last_name)values(?,?,?,?,?,?)";
                var values = [email, encryptPassword, accessToken, loginTime, firstName, lastName];

                connection.query(sql, values, function (err, userInsertResult) {
                    if (err) {

                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var data = {
                            access_token: accessToken,
                            first_name: firstName,
                            last_name: lastName
                        };
                        sendResponse.sendSuccessData(data, res);
                    }
                });

            }

    );
});

/*
 * --------------------------------------------------------------------------
 * login_user
 * INPUT : email, password
 * OUTPUT : error, userResult
 * ---------------------------------------------------------------------------
 */
router.post('/login_user', function (req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    var checkBlank = [email, password];
    var accessToken = "SELECT 'access_token' FROM users";
    async.waterfall(
            [function (callback) {
                    func.checkBlank(res, checkBlank, callback);
                }, function (callback) {
                    //console.log("inside here");
                    func.checkEmail(res, email, callback);
                },
                function (callback) {
                    func.accessTokenCheck(accessToken, res, callback);
                }],
            function (resultCallback) {

                var loginTime = new Date();
                var accessToken = func.encrypt(email + loginTime);
                var loginStatus = 1;
                var sql = "UPDATE `users` SET `access_token`=?,`last_login`=?,`login_status`=? WHERE `email`=? LIMIT 1";
                var values = [accessToken, loginTime, loginStatus,email];

                connection.query(sql, values, function (err, userInsertResult) {
                    if (err) {
                        console.log(err);
                        res.send("ERR");
                    }
                    else {

                        sendResponse.sendSuccessData("LOGGED IN", res);
                    }

                });

            });
});
/*
 * --------------------------------------------------------------------------
 * forgot password
 * INPUT : email
 * OUTPUT : error, userResult
 * ---------------------------------------------------------------------------
 */
router.post('/forgot_password', function (req, res, next) {
    var email = req.body.email;
    var mainValues = [email];
    func.checkBlank(res, mainValues, function (callback) {
        if (callback == null)
        {
            console.log("entering");
            var sql = "SELECT user_id FROM users WHERE email=? LIMIT 1";
            connection.query(sql, [email], function (err, response1) {
                if (err)
                {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    var id = [email];
                    var newPassword = passwordGenerate(7, false);
                    var encryptedPass = md5(newPassword);
                    var subject = "Password Reset";
                    var message = "This is a one time password.Please reset your password. Click on the given link to reset password";
                    message += encryptedPass ;
                    func.sendEmail(message, subject, id, function (callback)
                    {
                        if (callback == 0)
                        {
                            sendResponse.sendErrorMessage(constant.responseMessage.email_sending_failed, res);
                        }
                        else
                        {
                            var sql1 = "UPDATE `users` SET `one_time_link`=?,`password_reset`=? WHERE `user_id`=? LIMIT 1";
                            connection.query(sql1, [encryptedPass, 0, response1[0].user_id], function (err, result) {
                        
                                sendResponse.sendSuccessData(constant.responseMessage.email_sending, res);
                            });

                        }
                    });
                }
            });
        }
        else
        {
            sendResponse.unregisteredEmailMsg(constant.responseMessage.EMAIL_NOT_REGISTERED, res);
        }
    });
});
/*
 * --------------------------------------------------------------------------
 * Reset password
 * INPUT : one time link
 * OUTPUT : error, userResult
 * ---------------------------------------------------------------------------
 */
router.post('/reset_password', function (req, res, next) {
    var link = req.body.link;
    var passReset=req.body.password_reset;
    if(passReset==1){
        res.send("Link expired");
    }
    else{ passReset=1;
    var newPassword = req.body.new_password;
    var parameters = [link, newPassword];
    func.checkBlank(res, parameters, function (callback) {
        if (callback === null)
        {
            var sql = "SELECT `user_id` FROM `users` WHERE `one_time_link`=? LIMIT 1";
            connection.query(sql, [link], function (err, results) {
                console.log(err);
                if (results == 0) {
                    sendResponse.invalidAccessTokenError(res);
                }
                else{
                    var hash = md5(newPassword);
                    var sql2 = "UPDATE `users` SET `one_time_link`=? ,`password_reset`=?, `password`=? WHERE `user_id`=? LIMIT 1";
                    connection.query(sql2, ["",passReset, hash, results[0].user_id], function (err, result1) {
                        //console.log(err);
                        sendResponse.successStatusMsg("Password reset successful" ,res);  

                    });
                }
            });
        }
        else {
            sendResponse.somethingWentWrongError(res);

            }
    });
}
});
/*
 * --------------------------------------------------------------------------
 * logout
 * INPUT : email, password
 * OUTPUT : error, userResult
 * ---------------------------------------------------------------------------
 */
router.post('/logout', function (req, res, next) {
    var loginStatus = 0;
    var userId = req.body.user_id;
    console.log(userId);
    var accessToken = req.body.access_token;
    console.log(accessToken);
    func.accessTokenCheck(accessToken, res, function (callback)
    {
        var sql = "UPDATE users SET login_status=? WHERE user_id=? LIMIT 1";
        var values = [loginStatus, userId];
        console.log(values);
        connection.query(sql, values, function (err, userInsertResult) {
            if (err) {

                sendResponse.somethingWentWrongError(res);
            }
            else {
                //console.log(userInsertResult);
                sendResponse.logoutSuccess(constant.responseMessage.LOGOUT_SUCCESSFULL, res);
            }
        });
    });
});
module.exports = router;